import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireStorageModule  } from '@angular/fire/compat/storage';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from '../environments/environment';
import { AuthService } from './shared/services/auth.service';
import { UtilityService } from './shared/services/utility.service';
import { Login } from './shared/login/login.component';
import { Signup } from './shared/signup/signup.component';
import { MatDialogModule, MAT_DIALOG_DATA, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { ConstantService } from './shared/services/const.service';
import { File } from './shared/file/file.component';
import { FileUploadSuccess } from './shared/file-upload-success/file-upload-success.component';
import { ToastrModule } from 'ngx-toastr';
import { LoaderModule } from './shared/loader/loader.module';
import { UploadModule } from './components/upload/upload.module';
import { HomeModule } from './components/home/home.module';
import { EmailVerification } from './shared/email-verification/email-verification.component';
import { ForgotPassword } from './shared/forgot-password/forgot-password.component';
import { AdminModule } from './components/admin/admin.module';
import { FileAdmin } from './shared/file-admin/file-admin.component';
import { User } from './shared/user/user.component';
import { AboutModule } from './components/about/about.module';
import { ContactModule } from './components/contact/contact.module';
import { DownloadModule } from './components/download/download.module';
import { MarqueeModule } from './shared/marquee/marquee.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    Login,
    Signup,
    File,
    FileUploadSuccess,
    EmailVerification,
    ForgotPassword,
    FileAdmin,
    User
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    DownloadModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    LoaderModule,
    UploadModule,
    HomeModule,
    AdminModule,
    AboutModule,
    ContactModule,
    MarqueeModule
  ],
  providers: [{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {}},{ provide: MAT_DIALOG_DATA, useValue: {} }, AuthService, UtilityService, ConstantService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { UtilityService } from '../../shared/services/utility.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ConstantService } from '../../shared/services/const.service';
import { Observable, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../../shared/user/user.component';
import { FileAdmin } from 'app/shared/file-admin/file-admin.component';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-upload',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnInit {
    showSpinner: boolean;
    userData: any;
    isError: boolean;
    errorMsg: string;
    isLoggedIn: boolean;
    isAdmin: boolean;
    userAuthObs: Subscription;
    userDataObs: Subscription;
    firebaseDBObs: Observable<any>;
    documentArray: any;
    firebaseDBUsersObs: Observable<any>;
    userArray: any;
    notices: any;
    noticeSub: Subscription;
    noticeUploadsub: Subscription;

    constructor(private utilityService: UtilityService,
        private angularFireAuth: AngularFireAuth,
        private constantService: ConstantService,
        private dialog: MatDialog,
        public toastr: ToastrService) {
    
        this.userAuthObs = this.angularFireAuth.authState.subscribe(user => {
            if (user && user.email && user.emailVerified) {
                this.isLoggedIn = true;
                this.errorMsg = '';
                //this.utilityService.getUserDetailsFromDatabase(user.email);
            }
            else {
                this.showSpinner = false;
                this.isLoggedIn = false;
                this.errorMsg = this.constantService.nonLoggedInFileUploadErrorMsg;
            }
        });
        this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            if (user) {
                this.userData = user;
                this.showSpinner = false;
                if (user.isAdmin) {
                    this.isLoggedIn = true;
                    this.isAdmin = true;
                    this.getFilesFromDatabase();
                    this.getUsers();
                }
            }
        });
        this.noticeSub = this.utilityService.getNotices().subscribe((result: any) => {
            if (result) {
                this.notices = result.sort((a, b) => (new Date(b.date).getTime() - new Date(a.date).getTime()));
            }
        });

        this.noticeUploadsub = this.utilityService.noticeObservable.subscribe((result: any) => {
            if (result) {
                this.toastr.success('Announcement uploaded successfully', 'Success', {
                    timeOut: 3000,
                    closeButton: true
                });
                let form = document.getElementById('noticeForm') as HTMLFormElement;
                form.reset();
            }
        });
    }

    ngOnInit() {
        this.showSpinner = true;
    }

    getUsers() {
        this.firebaseDBUsersObs = this.utilityService.getUsersFromDatabase();
        this.firebaseDBUsersObs.subscribe((userList) => {
            this.userArray = userList;
        });
    }

    openUser(email: any) {
        let userData;
        this.userArray.forEach((user: any) => {
            if (user.email == email) {
                userData = user;
                this.dialog.open(User, {
                    height: '100vh',
                    width: '100vw',
                    panelClass: 'mat-dialog-modal',
                    data: userData
                });
            }
        });
    }

    getFilesFromDatabase() {
        this.showSpinner = true;
        this.firebaseDBObs = this.utilityService.getFileDetailsFromDatabase();
        this.firebaseDBObs.subscribe((fileList) => {
            this.documentArray = fileList;
            this.showSpinner = false;
        });
    }

    openDocument(documentId: any) {
        let fileData;
        this.documentArray.forEach((file: any) => {
            if (file.id == documentId) {
                fileData = file;
                this.dialog.open(FileAdmin, {
                    height: '100vh',
                    width: '100vw',
                    panelClass: 'mat-dialog-modal',
                    data: fileData
                });
            }
        });
    }

    uploadNotice() {
        let title = document.getElementById('title') as HTMLInputElement;
        let desc = document.getElementById('desc') as HTMLInputElement;
        let noticeObj = {
            title: title.value,
            desc: desc.value,
            username: this.userData.username,
            date: new Date().toString()
        };
        this.utilityService.insertNoticeIntoNoticeDB(noticeObj);
    }

    deleteNotice(noticeTitle: string) {
        this.utilityService.deleteNotice(noticeTitle);
    }

    ngOnDestroy() {
        if (this.userAuthObs) {
            this.userAuthObs.unsubscribe();
        }
        if (this.userDataObs) {
            this.userDataObs.unsubscribe();
        }
        if (this.noticeSub) {
            this.noticeSub.unsubscribe();
        }
        if (this.noticeUploadsub) {
            this.noticeUploadsub.unsubscribe();
        }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { LoaderModule } from '../../shared/loader/loader.module';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        NgbModule,
        ReactiveFormsModule,
        LoaderModule
    ],
    declarations: [
        AdminComponent
    ]
})
export class AdminModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DownloadComponent } from './download.component';
import { LoaderModule } from '../../shared/loader/loader.module';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        LoaderModule
    ],
    declarations: [ DownloadComponent ]
})
export class DownloadModule { }

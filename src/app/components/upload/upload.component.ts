import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from '@angular/fire/compat/storage'
import { finalize } from 'rxjs/operators';
import { UtilityService } from '../../shared/services/utility.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { ConstantService } from '../../shared/services/const.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})

export class UploadComponent implements OnInit {

    showSpinner: boolean;
    focus;
    focus1;
    date: { year: number, month: number };
    isUploaded: boolean;
    selectedFile: any;
    userData: any;
    isError: boolean;
    errorMsg: string;
    isLoggedIn: boolean;
    userAuthObs: Subscription;
    userDataObs: Subscription;
    fileUploadObs: Subscription;
    accessCodeObs: Subscription
    docuemntIdObs: Subscription;
    formValue: any;

    formTemplate = new FormGroup({
        name: new FormControl('', Validators.required),
        id: new FormControl('', Validators.required),
        desc: new FormControl('', Validators.required),
        url: new FormControl('', Validators.required)
    });

    constructor(private utilityService: UtilityService,
        private angularFireStorage: AngularFireStorage,
        private angularFireAuth: AngularFireAuth,
        private constantService: ConstantService) {

        this.userAuthObs = this.angularFireAuth.authState.subscribe(user => {
            if (user && user.email && user.emailVerified) {
                this.isLoggedIn = true;
                this.errorMsg = '';
            }
            else {
                this.isLoggedIn = false;
                this.errorMsg = this.constantService.nonLoggedInFileUploadErrorMsg;
                this.isDisabled(this.userData);
            }
        });
        this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            if(user) {
                this.userData = user;
                if(!user.isAdmin) {
                this.isLoggedIn = true;
                this.errorMsg = 'Only admin users can upload documents. Please contact us for further information.';
                this.isDisabled(null);
                }
                else {
                this.isDisabled(this.userData);
                }
            }
        });

        this.docuemntIdObs = this.utilityService.documentIdsObservable.subscribe((result: any) => {
            if (this.userData && this.formTemplate.valid && this.isUploaded) {
                if (result == 'existed') {
                    this.showSpinner = false;
                    this.errorMsg = 'Document Id already exist. Please enter unique Document Id.';
                }
                else if (result = 'notexisted') {
                    this.showSpinner = true;
                    this.errorMsg = '';
                    this.processFileUpload(this.formValue);
                }
            }
        });

        this.fileUploadObs = this.utilityService.fileUploadObservable.subscribe((result: string) => {
            if(result == 'uploaded') {
                this.showSpinner = false;
                this.errorMsg = '';
            }
            else if(result == 'failed') {
                this.showSpinner = false;
                this.errorMsg = 'File upload failled due to technical error. Please try again later.'
            }
        })
    }

    ngOnInit() {
        let input_group_focus = document.getElementsByClassName('form-control');
        let input_group = document.getElementsByClassName('input-group');
        for (let i = 0; i < input_group.length; i++) {
            input_group[i].children[0].addEventListener('focus', function () {
                input_group[i].classList.add('input-group-focus');
            });
            input_group[i].children[0].addEventListener('blur', function () {
                input_group[i].classList.remove('input-group-focus');
            });
        }
        this.resetForm();
    }

    fileChanged(event: any) {
        if (event.target.files && event.target.files[0]) {
            this.selectedFile = event.target.files[0];
        }
    }

    get formControls() {
        return this.formTemplate['controls'];
    }

    isDisabled(value) {
        if(!value) {
         this.formTemplate.controls['name'].disable();
         this.formTemplate.controls['id'].disable();
         this.formTemplate.controls['desc'].disable();
         this.formTemplate.controls['url'].disable();
        }
        else {
            this.formTemplate.controls['name'].enable();
            this.formTemplate.controls['id'].enable();
            this.formTemplate.controls['desc'].enable();
            this.formTemplate.controls['url'].enable();
        }
       }

    //function to upload files to firebase cloud storage
    upload(formValue: any) {
        this.isUploaded = true;
        if (this.formTemplate.valid) {
            this.showSpinner = true;
            this.formValue = formValue;
            this.utilityService.getDocumentIds(formValue['id']);            
        }
    }

    processFileUpload(formValue: any) {
        this.showSpinner = true;
        var filePath = `documents/${formValue.id}_${this.selectedFile.name}_${(new Date().getTime())}`;
        let fileRef = this.angularFireStorage.ref(filePath);
        this.angularFireStorage.upload(filePath, this.selectedFile).snapshotChanges().pipe(
            finalize(() => {
                fileRef.getDownloadURL().subscribe((url: any) => {
                    formValue['url'] = url;
                    formValue['date'] = this.utilityService.getCurrentDate();
                    formValue['time'] = this.utilityService.getcurrentTime();
                    formValue['email'] = this.userData?.email;
                    formValue['username'] = this.userData?.username;
                    this.utilityService.insertFileDetailsIntoDatabase(formValue);
                    this.resetForm();
                });
            })
        ).subscribe(()=>{
        },(error) => {
            this.errorMsg = error.message;
            if(this.errorMsg.includes(':')) {
                this.errorMsg = this.errorMsg.split(':')[1];

            }
        });
    }

    resetForm() {
        this.isUploaded = false;
        this.selectedFile = null;
        this.formTemplate.reset();
        this.formTemplate.setValue({
            name: '',
            id: '',
            desc: '',
            url: '',
        });
    }

    ngOnDestroy() {
        if(this.userAuthObs) {
            this.userAuthObs.unsubscribe();
        }
        if(this.userDataObs) {
            this.userDataObs.unsubscribe();
        }
        if(this.fileUploadObs) {
            this.fileUploadObs.unsubscribe();
        }
        if(this.accessCodeObs) {
            this.accessCodeObs.unsubscribe();
        }
    }
}
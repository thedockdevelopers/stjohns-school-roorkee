import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UploadComponent } from './upload.component';
import { LoaderModule } from '../../shared/loader/loader.module';
import { MarqueeModule } from '../../shared/marquee/marquee.module'; 
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        LoaderModule,
        MarqueeModule
    ],
    declarations: [
        UploadComponent
    ],
    entryComponents: [],
    exports:[ UploadComponent ]
})
export class UploadModule { }
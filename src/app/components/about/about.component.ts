import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilityService } from 'app/shared/services/utility.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit {

    firebaseDBObs: Observable<any>;
    documentArray: any;

    constructor(
        private utilityService: UtilityService,
        private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.firebaseDBObs = this.utilityService.getFileDetailsFromDatabase();
        this.firebaseDBObs.subscribe((fileList) => {
            this.documentArray = fileList;
        });
    }

    docUrl(url: string) {
        let docUrl = 'https://docs.google.com/gview?url='+url+'&embedded=true';
        return this.sanitizer.bypassSecurityTrustResourceUrl(
            url
        );
      }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
    imports: [
        CommonModule,
        BrowserModule
    ],
    declarations: [ContactComponent],
    entryComponents: [],
    exports: [ContactComponent]
})
export class ContactModule { }

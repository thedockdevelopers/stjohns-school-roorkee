import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import { fromLonLat } from 'ol/proj.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import VectorSource from 'ol/source/Vector';
import {Icon, Style} from 'ol/style';
import OSM from 'ol/source/OSM';
import {Attribution, defaults as defaultControls} from 'ol/control';
import Overlay from 'ol/Overlay';
@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {
  map: any;

  ngOnInit() {
    let place = new Feature({
      geometry: new Point(fromLonLat([77.88111885480453, 29.88696933040617])),
    });
    place.set('name','St Johns')
    place.setStyle(new Style({
      image: new Icon({
        color: 'darkred',
        crossOrigin: 'anonymous',
        src: 'http://maps.google.com/mapfiles/ms/icons/red.png',
        imgSize: [40, 40],
        opacity: 1
      })
    }));

    let vectorSource = new VectorSource({
      features: [place]
    });

    let vectorLayer = new VectorLayer({
      source: vectorSource
    });

   this.map = new Map({
      target: 'hotel_map',
      layers: [
        new TileLayer({
          source: new OSM()
        }),
        vectorLayer
      ],
      view: new View({
        center: fromLonLat([77.88111885480453, 29.88696933040617]),
        zoom: 13
      }),
      controls: defaultControls({attribution: false, zoom: false, rotate: false, }),
    });

    var container = document.getElementById('popup');
    var overlay = new Overlay({
      element: container
  });
  overlay.setPosition(fromLonLat([77.88111885480453, 29.88696933040617]));
  this.map.addOverlay(overlay);
  }
}

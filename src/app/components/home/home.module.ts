import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMarqueeModule } from 'ng-marquee';
import { CommonModule } from '@angular/common';
@NgModule({
    declarations: [ HomeComponent ],
    imports: [ NgbModule, NgMarqueeModule, CommonModule ],
    entryComponents: [ HomeComponent ] 
})

export class HomeModule {}
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UtilityService } from '../../shared/services/utility.service';
@Component({
    selector: 'home',
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit, OnDestroy {

    @ViewChild('carousel') carousel: NgbCarousel;
    notices: any;
    noticeSub: Subscription;

    constructor(
        private utilityService: UtilityService
    ) { }

    ngOnInit() {
        this.noticeSub = this.utilityService.getNotices().subscribe((result: any) => {
            if (result) {
                this.notices = result.sort((a, b) => (new Date(b.date).getTime() - new Date(a.date).getTime()));
            }
        });
    }

    prev() {
        this.carousel.prev();
    }

    next() {
        this.carousel.next();
    }

    ngOnDestroy() {
        if (this.noticeSub) {
            this.noticeSub.unsubscribe();
        }
    }
}
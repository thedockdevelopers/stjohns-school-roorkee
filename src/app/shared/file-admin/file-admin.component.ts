import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'app-file-admin',
    templateUrl: 'file-admin.component.html',
    styleUrls: ['file-admin.component.scss']
})

export class FileAdmin implements OnInit {

    fileName: string;
    fileDesc: string;
    fileUrl: string;
    fileRoute: string;
    copyButtonText = 'Copy Link';
    copied: boolean;
    showSpinner: boolean;

    constructor(public dialogRef: MatDialogRef<File>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public router: Router,
        private angularFireStorage: AngularFireStorage,
        private angularFirestore: AngularFirestore,
        public toastr: ToastrService) {
    }

    ngOnInit() {
        let host = window.location.protocol + '//' + window.location.host;
        this.fileName = this.data ? this.data.name : 'Not Found';
        this.fileDesc = this.data ? this.data.desc : 'We didn\'t find any file with the provided ID';
        this.fileUrl = this.data ? this.data.url : '';
        this.fileRoute = this.data ? (host + '/download/') + this.data.id : '';
    }

    closeDialog() {
        this.dialogRef.close();
    }

    copyLink() {
        let copyText = document.getElementById('linkText');
        let selection = window.getSelection();
        let range = document.createRange();
        range.selectNodeContents(copyText);
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand('Copy');
        this.copied = true;
        this.copyButtonText = 'Copied';
    }

    deleteDocument() {
        this.showSpinner = true;
        if (this.fileUrl) {
            let fileRef = this.angularFireStorage.refFromURL(this.fileUrl);
            fileRef.delete().toPromise().then(() => {
                this.angularFirestore.collection('documents').doc(this.data.id).delete().then(() => {
                    this.showSpinner = false;
                    this.toastr.success('Document is successfully deleted', 'Success', {
                        timeOut: 3000,
                        closeButton: true
                    });
                    this.closeDialog();
                }).catch(() => {
                    this.showSpinner = false;
                    this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                        timeOut: 3000,
                        closeButton: true
                    });
                });
            }).catch(() => {
                this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                    timeOut: 3000,
                    closeButton: true
                });
            });
        }
    }
}
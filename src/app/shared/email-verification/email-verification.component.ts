import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
    selector: 'app-email-verification',
    templateUrl: 'email-verification.component.html',
    styleUrls: ['email-verification.component.scss']
})

export class EmailVerification {

    showSpinner: boolean;
    errorMessage: any;
    verificationMessage: string;

    constructor(
        private dialogRef: MatDialogRef<EmailVerification>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private angularFireAuth: AngularFireAuth) {
    }

    sendVerificationEmail() {
        this.showSpinner = true;
        this.errorMessage = false;
        this.verificationMessage = '';
        return this.angularFireAuth.currentUser.then(u => u.sendEmailVerification())
            .then(() => {
                this.showSpinner = false;
                this.verificationMessage = 'We have successfully sent verification link to your email';
            }).catch((error) => {
                this.showSpinner = false;
                this.errorMessage = error.message;
                if (this.errorMessage.includes(':')) {
                    this.errorMessage = this.errorMessage.split(':')[1];

                }
            });
    }

    closeDialog() {
        this.dialogRef.close();
    }
}

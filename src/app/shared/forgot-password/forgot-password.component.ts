import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MatDialogRef } from '@angular/material/dialog';
import { AngularFireAuth } from '@angular/fire/compat/auth';
@Component({
    selector: 'app-forgot-password-form',
    templateUrl: 'forgot-password.component.html',
    styleUrls: ['forgot-password.component.scss']
})

export class ForgotPassword {

    showSpinner: boolean;
    isSubmitted: boolean;
    errorMessage: any;
    verificationMessage: string;
    loginForm = new FormGroup({
        email: new FormControl('', Validators.compose([Validators.required, 
            Validators.pattern(RegularExpressionConstant.EMAIL)]))
    });

    constructor(
        public toastr: ToastrService,
        private dialogRef: MatDialogRef<ForgotPassword>,
        private angularFireAuth: AngularFireAuth) {
    }

    get formControls() {
        return this.loginForm['controls'];
    }

    resetPassword(formValue: any) {
        this.showSpinner = true;
        return this.angularFireAuth.sendPasswordResetEmail(formValue.email)
        .then(() => {
            this.resetForm();
            this.verificationMessage = 'We have successfully sent verification link to '+formValue.email;
        }).catch((error) => {
            this.showSpinner = false;
            this.errorMessage = error.message;
                if(this.errorMessage.includes(':')) {
                    this.errorMessage = this.errorMessage.split(':')[1];

                }
            });
    }

    resetForm() {
        this.loginForm.reset();
        this.loginForm.setValue({
            email: ''
        });
        this.isSubmitted = false;
        this.errorMessage = false;
        this.showSpinner = false;
    }

    closeDialog() {
        this.resetForm();
        this.dialogRef.close();
    }
}

export class RegularExpressionConstant {
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}
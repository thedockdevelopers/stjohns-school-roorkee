import { Component, NgZone } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ForgotPassword } from '../forgot-password/forgot-password.component';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { UtilityService } from '../services/utility.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
@Component({
    selector: 'app-login-form',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})

export class Login {

    userDataObs: Subscription;
    showSpinner: boolean;
    isSubmitted: boolean;
    errorMessage: any;
    fieldTextType: boolean;
    emailVerifyError: boolean;
    sentLink: boolean;
    loginForm = new FormGroup({
        email: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern(RegularExpressionConstant.EMAIL)])),
        password: new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(8)]))
    });
    loginClicked: boolean;

    constructor(
        public toastr: ToastrService,
        private dialogRef: MatDialogRef<Login>,
        private dialog: MatDialog,
        private angularFireAuth: AngularFireAuth,
        public ngZone: NgZone,
        private utilityService: UtilityService,
        private authService: AuthService) {

        this.userDataObs = this.utilityService.userObservable.subscribe((user: any) => {
            if (user) {
                if (user.isActive) {
                    this.closeDialog();
                    this.toastr.success('You are successfully logged in', 'Success', {
                        timeOut: 3000,
                        closeButton: true
                    });
                }
                else {
                    this.resetForm();
                    this.errorMessage = 'You are blocked by the administration. Please contact school authorities.';
                    setTimeout(() => {
                        this.authService.signOut();
                    }, 2000);
                }
            }
            else {
                this.resetForm();
                this.errorMessage = 'There is a problem with your account. Please contact school authorities.';
            }
        });
    }

    get formControls() {
        return this.loginForm['controls'];
    }

    showPassword() {
        this.fieldTextType = !this.fieldTextType;
    }

    resetForm() {
        this.loginForm.reset();
        this.loginForm.setValue({
            email: '',
            password: ''
        });
        this.isSubmitted = false;
        this.errorMessage = false;
        this.showSpinner = false;
    }

    closeDialog() {
        this.resetForm();
        this.dialogRef.close();
    }

    login(formValue: any) {
        this.loginClicked = true;
        this.isSubmitted = true;
        this.errorMessage = false;
        this.emailVerifyError = false;
        this.sentLink = false;
        if (this.loginForm.valid) {
            this.showSpinner = true;
            return this.angularFireAuth.signInWithEmailAndPassword(formValue.email, formValue.password)
                .then((result) => {
                    if (result) {
                        this.ngZone.run(() => {
                            this.showSpinner = false;
                            if (!result.user.emailVerified) {
                                this.resetForm();
                                this.errorMessage = 'Please verify your email address';
                                this.emailVerifyError = true;
                            }
                        });
                    }
                }).catch((error) => {
                    this.showSpinner = false;
                    this.errorMessage = error.message;
                    if (this.errorMessage.includes(':')) {
                        this.errorMessage = this.errorMessage.split(':')[1];

                    }
                });
        }
    }

    sendVerificationEmail() {
        this.showSpinner = true;
        this.errorMessage = false;
        return this.angularFireAuth.currentUser.then(u => u.sendEmailVerification())
            .then(() => {
                this.showSpinner = false;
                this.sentLink = true;
                this.errorMessage = 'We have successfully sent verification link to your email';
            }).catch((error) => {
                this.showSpinner = false;
                this.errorMessage = error.message;
                if (this.errorMessage.includes(':')) {
                    this.errorMessage = this.errorMessage.split(':')[1];

                }
            });
    }

    openForgotPasswordDialog() {
        this.closeDialog();
        this.dialog.open(ForgotPassword, {
            height: '100vh',
            width: '100vw',
            panelClass: 'mat-dialog-modal'
        });
    }
}
export class RegularExpressionConstant {
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}
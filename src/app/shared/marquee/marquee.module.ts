import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarqueeComponent } from './marquee.component';
import { NgMarqueeModule } from 'ng-marquee';

@NgModule({
    imports: [
        CommonModule,
        NgMarqueeModule
    ],
    declarations: [
        MarqueeComponent
    ],
    entryComponents: [MarqueeComponent],
    exports: [MarqueeComponent]
})
export class MarqueeModule { }

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscriber, Subscription } from 'rxjs';
import { UtilityService } from '../services/utility.service';

@Component({
  selector: 'app-marquee',
  templateUrl: './marquee.component.html',
  styleUrls: ['./marquee.component.scss']
})

export class MarqueeComponent implements OnInit, OnDestroy {

  notices: any;
  noticeSub: Subscription;

  constructor(
    private utilityService: UtilityService
  ) { }

  ngOnInit() {
    this.noticeSub = this.utilityService.getNotices().subscribe((result: any) => {
      if (result) {
        this.notices = result.sort((a, b) => (new Date(b.date).getTime() - new Date(a.date).getTime()));
      }
    });
  }

  ngOnDestroy() {
    if (this.noticeSub) {
      this.noticeSub.unsubscribe();
    }
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { UtilityService } from '../services/utility.service';
import { ToastrService } from 'ngx-toastr';
import { EmailVerification } from '../email-verification/email-verification.component';
import { AngularFireAuth } from '@angular/fire/compat/auth';
@Component({
    selector: 'app-signup-form',
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.scss']
})

export class Signup {

    showSpinner: boolean;
    errorMsg: string;
    isSubmitted: boolean;
    signupErrorObs: Subscription;
    userDataObs: Subscription;
    roles = ['Student', 'Teacher'];
    isAdmin: boolean;
    user: any;
    formValue: any;
    signupSuccessObs: Subscription;
    userPostObs: Subscription;
    fieldTextType: boolean;
    signupForm = new FormGroup({
        username: new FormControl('', Validators.required),
        email: new FormControl('', Validators.compose([Validators.required,
        Validators.pattern(RegularExpressionConstant.EMAIL)])),
        password: new FormControl('', Validators.compose([Validators.required,
        Validators.minLength(8)])),
        role: new FormControl('', Validators.required)
    });

    constructor(public dialogRef: MatDialogRef<Signup>,
        private utilityService: UtilityService,
        public toastr: ToastrService,
        private dialog: MatDialog,
        private angularFireAuth: AngularFireAuth) {}

    showPassword() {
        this.fieldTextType = !this.fieldTextType;
    }

    get formControls() {
        return this.signupForm['controls'];
    }

    resetForm() {
        this.signupForm.setValue({
            username: '',
            email: '',
            password: '',
            role: ''
        });
        this.isSubmitted = false;
        this.errorMsg = '';
        this.showSpinner = false;
    }

    closeDialog() {
        this.showSpinner = false;
        this.resetForm();
        this.dialogRef.close();
    }

    changeRole(e) {
        this.signupForm.controls['role'].setValue(e.target.value);
    }

    signup(formValue: any) {
        this.isSubmitted = true;
        this.errorMsg = '';
        if (this.signupForm.valid) {
            this.showSpinner = true;
            formValue['date'] = this.utilityService.getCurrentDate();
            formValue['time'] = this.utilityService.getcurrentTime();
            formValue['usertype'] = 'normal';
            this.formValue = formValue;
            return this.angularFireAuth.createUserWithEmailAndPassword(formValue.email, formValue.password)
                .then((result) => {
                    if (result) {
                        this.SendVerificationMail();
                    }
                }).catch((error) => {
                    this.showSpinner = false;
                    this.errorMsg = error.message
                    if (this.errorMsg.includes(':')) {
                        this.errorMsg = this.errorMsg.split(':')[1];
                    }
                })
        }
    }

    SendVerificationMail() {
        return this.angularFireAuth.currentUser.then(u => u.sendEmailVerification())
            .then(() => {
                this.showSpinner = false;
                this.formValue['role'] = this.formValue['role'].split(':')[1];
                delete this.formValue['password'];
                this.formValue['usertype'] = 'normal';
                this.formValue['isActive'] = true;
                this.utilityService.insertUserDetailsIntoDatabase(this.formValue);
                this.closeDialog();
                this.openEmailVerificationDialog();
            }).catch((error) => {
                this.errorMsg = error.message;
                if (this.errorMsg.includes(':')) {
                    this.errorMsg = this.errorMsg.split(':')[1];

                }
            });
    }

    openEmailVerificationDialog() {
        this.dialog.open(EmailVerification, {
            height: '100vh',
            width: '100vw',
            panelClass: 'mat-dialog-modal',
            data: {
                email: this.formValue.email
            }
        });
    }
}
export class RegularExpressionConstant {
    static EMAIL: RegExp = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
}
import { not } from '@angular/compiler/src/output/output_ast';
import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileUploadSuccess } from '../file-upload-success/file-upload-success.component';
@Injectable({
  providedIn: 'root'
})

export class UtilityService implements OnDestroy {

  fileDetailList: AngularFireList<any>;
  private userSubject = new BehaviorSubject<any>(null);
  userObservable: Observable<boolean> = this.userSubject.asObservable();
  firestoreSubscription: Subscription;
  private fileUploadSubject = new BehaviorSubject<any>(null);
  fileUploadObservable: Observable<any> = this.fileUploadSubject.asObservable();
  private userDataPostSubject = new BehaviorSubject<any>(null);
  userDataPostObservable: Observable<any> = this.userDataPostSubject.asObservable();
  private documentIdsSubject = new BehaviorSubject<any>(null);
  documentIdsObservable: Observable<any> = this.documentIdsSubject.asObservable();
  private noticeSubject = new BehaviorSubject<any>(null);
  noticeObservable: Observable<any> = this.noticeSubject.asObservable();

  constructor(
    private angularFirestore: AngularFirestore,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  insertFileDetailsIntoDatabase(fileDetails: any) {
    this.angularFirestore.collection('documents').doc(fileDetails.id).set(fileDetails).then(() => {
      this.fileUploadSubject.next('uploaded');
      console.log('File uploaded successfully');
      this.dialog.open(FileUploadSuccess, {
        height: '100vh',
        width: '100vw',
        panelClass: 'mat-dialog-modal',
        data: fileDetails
      });
    },
    (error) => {
      console.log(error.message);
      this.fileUploadSubject.next('failed');
    })
  }

  getFileDetailsFromDatabase(): Observable<any> {
    return this.angularFirestore.collection('documents').valueChanges();
  }

  getUsersFromDatabase(): Observable<any> {
    return this.angularFirestore.collection('users').valueChanges();
  }

  getUserDetailsFromDatabase(email: string) {
    this.firestoreSubscription = this.angularFirestore.collection('users').doc(email).valueChanges().subscribe((user:any) => {
      if(user && user.email == email) {
        this.userSubject.next(user);
      }
    });
  }

  getDocumentIds(id: any){
    return this.angularFirestore.collection('documents').doc(id).valueChanges().subscribe((document: any) => {
      if(document) {
        this.documentIdsSubject.next('existed');
      }
      else {
        this.documentIdsSubject.next('notexisted');
      }
    });
  }

  insertUserDetailsIntoDatabase(userDetails: any) {
    this.angularFirestore.collection('users').doc(userDetails.email).set(userDetails).then(()=>{
      console.log('User added successfully');
    },(error)=>{
      console.log(error.message);
    });
  }

  getCurrentDate() {
    let today = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1;
    let yyyy: any = today.getFullYear();

    if (dd < 10) {
      dd = '0' + String(dd);
    }
    if (mm < 10) {
      mm = '0' + String(mm);
    }
    let currentDate = dd + '/' + mm + '/' + String(yyyy);
    return currentDate;
  }

  getcurrentTime() {
    let today = new Date();
    let hh = String(today.getHours());
    let mm = String(today.getMinutes());
    let ss = String(today.getSeconds());
    return hh+':'+mm+':'+ss;
  }

  insertNoticeIntoNoticeDB(noticeObj: any) {
    this.angularFirestore.collection('notices').doc(noticeObj.title).set(noticeObj).then(() => {
      this.noticeSubject.next(true);
      console.log('Announcement uploaded successfully');
    },(error) => {
      console.log(error.message);
    });
  }

  deleteNotice(noticeTitle: string) {
    this.angularFirestore.collection('notices').doc(noticeTitle).delete().then(() => {
      this.toastr.success('Announcement deleted successfully', 'Success', {
        timeOut: 3000,
        closeButton: true
      });
    }, (error) => {
      console.log(error.message);
      this.toastr.error('We are facing some technical error. Please try again after sometime', 'Error', {
        timeOut: 3000,
        closeButton: true
      });
    });
  }

  getNotices() {
    return this.angularFirestore.collection('notices').valueChanges();
  }

  ngOnDestroy() {
    if(this.firestoreSubscription) {
      this.firestoreSubscription.unsubscribe();
    }
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'app-user',
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.scss']
})

export class User implements OnInit {

    userType: string;
    userStatus: string;
    showSpinner: boolean;

    constructor(public dialogRef: MatDialogRef<File>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private angularFirestore: AngularFirestore,
        public toastr: ToastrService) {
    }

    ngOnInit() {
        this.userType = this.data.isAdmin ? 'Admin' : 'Non Admin';
        this.userStatus = this.data.isActive ? 'Active' : 'Blocked';
    }

    closeDialog() {
        this.dialogRef.close();
    }

    blockUser(email: any) {
        this.showSpinner = true;
        let userObject = this.data;
        userObject['isActive'] = false;
        this.angularFirestore.collection('users').doc(email).set(userObject).then(() => {
            this.showSpinner = false;
            this.userStatus = 'Blocked';
            this.toastr.success('User is now blocked', 'Success', {
                timeOut: 3000,
                closeButton: true
              });
        }).catch(() => {
            this.showSpinner = false;
            this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                timeOut: 3000,
                closeButton: true
              });
        });
    }
    
    unBlockUser(email: any) {
        this.showSpinner = true;
        let userObject = this.data;
        userObject['isActive'] = true;;
        this.angularFirestore.collection('users').doc(email).set(userObject).then(() => {
            this.showSpinner = false;
            this.userStatus = 'Active';
            this.toastr.success('User is now active', 'Success', {
                timeOut: 3000,
                closeButton: true
              });
        }).catch(() => {
            this.showSpinner = false;
            this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                timeOut: 3000,
                closeButton: true
              });
        });
    }

    makeAdmin(email: any) {
        this.showSpinner = true;
        let userObject = this.data;
        userObject['isAdmin'] = true;
        this.angularFirestore.collection('users').doc(email).set(userObject).then(() => {
            this.showSpinner = false;
            this.userType = 'Admin';
            this.toastr.success('User is now an admin', 'Success', {
                timeOut: 3000,
                closeButton: true
              });
        }).catch((error) => {
            this.showSpinner = false;
            this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                timeOut: 3000,
                closeButton: true
              });
        });
    }

    removeAdmin(email: any) {
        this.showSpinner = true;
        let userObject = this.data;
        userObject['isAdmin'] = false;
        this.angularFirestore.collection('users').doc(email).set(userObject).then(() => {
            this.showSpinner = false;
            this.userType = 'Non Admin';
            this.toastr.success('User is not admin anymore', 'Success', {
                timeOut: 3000,
                closeButton: true
              });
        }).catch(() => {
            this.showSpinner = false;
            this.toastr.error('We are facing some technical issue. Please try again.', 'Error', {
                timeOut: 3000,
                closeButton: true
              });
        });
    }
}